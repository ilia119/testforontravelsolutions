package by.igoroshko.tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class BaseTest {
    public static final String username = "igoroshko@tut.by";
    public static final String password = "testtest5";
    public static final String startUrl = "https://tut.by";

    private WebDriver driver;


    @BeforeMethod
    public void prepareTest(){
        System.setProperty("webdriver.chrome.driver", "src/test/java/artefacts/chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    }

    @Test
    public void baseLineTest(){
        driver.get(startUrl);
        driver.manage().window().maximize();
        driver.findElement(By.className("enter")).click();
        driver.findElement(By.name("login")).sendKeys(username);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.cssSelector("#authorize > div > div > div > form > div:nth-child(5) > input")).click();
        driver.findElement(By.cssSelector("#mainmenu > ul > li:nth-child(3) > a")).click();
        System.out.println("Count of emails in Inbox folder: " + driver.findElement(By.className("mail-NestedList-Item-Info-Extras")).getText());
    }

    @AfterMethod
    public void closeBrowser(){
        driver.close();
        driver.quit();
    }


}
